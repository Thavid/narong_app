package com.balancika.library;

import android.annotation.SuppressLint;
import android.content.SharedPreferences;

public class UserManager {
    private static SharedPreferences sharedPreferences;
    private static SharedPreferences.Editor editor;
    private static UserManager userManager = new UserManager();

    @SuppressLint("CommitPrefEdits")
    public static UserManager getInstance(SharedPreferences mPref) {
        sharedPreferences = mPref;
        editor = mPref.edit();
        return userManager;
    }

    public void saveUser(String userName){
        editor.putString("USER_NAME",null);
        editor.apply();
    }
    public String getUser(){return sharedPreferences.getString("USER_NAME",null);}
}
