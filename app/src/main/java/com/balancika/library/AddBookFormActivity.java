package com.balancika.library;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.balancika.library.api.Api;
import com.balancika.library.request.AddBookRequest;
import com.balancika.library.response.AddBookResponse;
import com.balancika.library.response.DataEntity;
import com.balancika.library.retrofit.ServiceGenerator;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddBookFormActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener, Validator.ValidationListener {
    @BindView(R.id.btn_done)
    TextView btnDone;
    @BindView(R.id.toolbar_title)
    TextView tvToolbarTitle;
    @BindView(R.id.type_spinner)
    Spinner typeSpinner;
    @NotEmpty
    @BindView(R.id.ed_book_title)
    EditText tvBookTitle;
    @NotEmpty
    @BindView(R.id.ed_author)
    EditText tvAuthor;
    @BindView(R.id.ed_date_picker)
    TextView btnDate;
    @BindView(R.id.progressBar)
    ProgressBar progress;
    @BindView(R.id.btn_confirm)
    Button btnConfirm;
    final Calendar myCalendar = Calendar.getInstance();
    String[] listType={"Comedy","Novel","Poems","Short stories"};
    String bookType;
    Validator validator;
    String type;
    DataEntity bookData;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_book_form);
        ButterKnife.bind(this);

        validator = new Validator(this);
        validator.setValidationListener(this);
        btnDone.setVisibility(View.INVISIBLE);
        tvToolbarTitle.setText("Add Book");

        progress.setVisibility(View.GONE);
        typeSpinner.setOnItemSelectedListener(this);
        ArrayAdapter aa = new ArrayAdapter(this,android.R.layout.simple_spinner_item,listType);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        typeSpinner.setAdapter(aa);

        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                String myFormat = "dd-MMM-yyyy"; //In which you need put here
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                btnDate.setText(sdf.format(myCalendar.getTime()));
            }
        };

        btnDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                new DatePickerDialog(AddBookFormActivity.this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        type = getIntent().getStringExtra("TYPE");
        if(type.equals("update")){
            btnConfirm.setText("Update");
            bookData = (DataEntity) getIntent().getSerializableExtra("BOOKDATA");
            tvBookTitle.setText(bookData.getTitle());
            bookType = bookData.getGenre();
            Log.e("oooooooooooooooo-",bookData.getGenre());
            for(int i=0;i<listType.length;i++){
                if(listType[i].equals(bookData.getGenre()))
                    typeSpinner.setSelection(i);
            }
            tvAuthor.setText(bookData.getAuthor());
            btnDate.setText(bookData.getRelease());
        }
    }

    @OnClick(R.id.btn_back)
    public void finishScreen(){
        finish();
    }
    @OnClick(R.id.btn_confirm)
    public void addBook(){
        progress.setVisibility(View.VISIBLE);
        validator.validate();
    }
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        bookType = listType[position];
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onValidationSucceeded() {
        AddBookRequest addBookRequest = new AddBookRequest(tvBookTitle.getText().toString(),btnDate.getText().toString(),bookType,tvAuthor.getText().toString());
        Api.BookApi api = ServiceGenerator.createService(Api.BookApi.class);
        if(type.equals("new")){
            Call<AddBookResponse> call = api.addBook(addBookRequest);
            call.enqueue(new Callback<AddBookResponse>() {
                @Override
                public void onResponse(Call<AddBookResponse> call, Response<AddBookResponse> response) {
                    if(response.isSuccessful()){
                        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(AddBookFormActivity.this);
                        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                        @SuppressLint("InflateParams") View dialogView = inflater.inflate(R.layout.cu_success_dialog, null);
                        alertDialog.setView(dialogView);

                        TextView tvTitle = dialogView.findViewById(R.id.tv_title);
                        TextView tvDescription = dialogView.findViewById(R.id.tv_description);
                        Button btnConfirm = dialogView.findViewById(R.id.btn_confirm);

                        tvTitle.setText("Success");
                        tvDescription.setText(response.body().getMessage());
                        btnConfirm.setText("Ok");

                        final AlertDialog dialog = alertDialog.create();
                        Objects.requireNonNull(dialog.getWindow()).getAttributes().windowAnimations = R.style.DialogAnimation;

                        btnConfirm.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                //callback.confirm();
                                dialog.dismiss();
                                finish();
                            }
                        });
                        dialog.show();
                    }else {
                        Toast.makeText(AddBookFormActivity.this, "Add book failed", Toast.LENGTH_SHORT).show();
                    }
                    progress.setVisibility(View.GONE);
                }

                @Override
                public void onFailure(Call<AddBookResponse> call, Throwable t) {
                    progress.setVisibility(View.GONE);
                }
            });
        }else {
            AddBookRequest updateBookRequest = new AddBookRequest(tvBookTitle.getText().toString(),bookData.getId(),btnDate.getText().toString(),bookType,tvAuthor.getText().toString());
            Call<AddBookResponse> call = api.updateBook(updateBookRequest);
            call.enqueue(new Callback<AddBookResponse>() {
                @Override
                public void onResponse(Call<AddBookResponse> call, Response<AddBookResponse> response) {
                    if(response.isSuccessful()){
                        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(AddBookFormActivity.this);
                        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                        @SuppressLint("InflateParams") View dialogView = inflater.inflate(R.layout.cu_success_dialog, null);
                        alertDialog.setView(dialogView);

                        TextView tvTitle = dialogView.findViewById(R.id.tv_title);
                        TextView tvDescription = dialogView.findViewById(R.id.tv_description);
                        Button btnConfirm = dialogView.findViewById(R.id.btn_confirm);

                        tvTitle.setText("Success");
                        tvDescription.setText(response.body().getMessage());
                        btnConfirm.setText("Ok");

                        final AlertDialog dialog = alertDialog.create();
                        Objects.requireNonNull(dialog.getWindow()).getAttributes().windowAnimations = R.style.DialogAnimation;

                        btnConfirm.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                //callback.confirm();
                                dialog.dismiss();
                                finish();
                            }
                        });
                        dialog.show();
                    }else {
                        Toast.makeText(AddBookFormActivity.this, "Update book failed", Toast.LENGTH_SHORT).show();
                    }
                    progress.setVisibility(View.GONE);
                }

                @Override
                public void onFailure(Call<AddBookResponse> call, Throwable t) {
                    Toast.makeText(AddBookFormActivity.this, "Update book failed", Toast.LENGTH_SHORT).show();
                    progress.setVisibility(View.GONE);
                }
            });
        }
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }
    }
}
