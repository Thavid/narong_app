package com.balancika.library;


import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.balancika.library.adapter.StudentAdapter;
import com.balancika.library.api.Api;
import com.balancika.library.response.DeleteResponse;
import com.balancika.library.response.StudentData;
import com.balancika.library.response.StudentResponse;
import com.balancika.library.retrofit.ServiceGenerator;
import com.balancika.library.util.SpacesItemDecoration;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class StudentFragment extends Fragment {
    @BindView(R.id.rv_student)
    RecyclerView rvStudent;
    @BindView(R.id.progress)
    ProgressBar progressBar;
    private StudentAdapter adapter;
    private List<StudentData> listStudent = new ArrayList<>();
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_student, container, false);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        listStudent.clear();
        Api.StudentApi api = ServiceGenerator.createService(Api.StudentApi.class);
        Call<StudentResponse> call = api.getStudent();
        call.enqueue(new Callback<StudentResponse>() {
            @Override
            public void onResponse(Call<StudentResponse> call, Response<StudentResponse> response) {
                if(response.isSuccessful()){
                    listStudent.addAll(response.body().data);
                    adapter.notifyDataSetChanged();
                }
                else {
                    Toast.makeText(getActivity(), getResources().getString(R.string.fail_message), Toast.LENGTH_SHORT).show();
                }
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<StudentResponse> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this,view);

        progressBar.setVisibility(View.VISIBLE);
        adapter = new StudentAdapter(getActivity(),listStudent,this);
        GridLayoutManager layoutManager = new GridLayoutManager(getActivity(),2);
        rvStudent.addItemDecoration(new SpacesItemDecoration(2,20,false));
        rvStudent.setLayoutManager(layoutManager);
        rvStudent.setAdapter(adapter);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void showDeleteDialog(final int i){
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        @SuppressLint("InflateParams") View dialogView = inflater.inflate(R.layout.confirm_dialog, null);
        alertDialog.setView(dialogView);

        TextView tvTitle = dialogView.findViewById(R.id.tv_title);
        TextView tvDescription = dialogView.findViewById(R.id.tv_description);
        Button btnCancel = dialogView.findViewById(R.id.btn_done);
        Button btnConfirm = dialogView.findViewById(R.id.btn_confirm);

        tvTitle.setText("Confirm");
        tvDescription.setText("Are you sure to delete?");
        btnConfirm.setText("Delete");

        final AlertDialog dialog = alertDialog.create();
        Objects.requireNonNull(dialog.getWindow()).getAttributes().windowAnimations = R.style.DialogAnimation;
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //callback.confirm();
                Api.StudentApi api = ServiceGenerator.createService(Api.StudentApi.class);
                Call<DeleteResponse> call = api.deleteStudent(listStudent.get(i).getId());
                call.enqueue(new Callback<DeleteResponse>() {
                    @Override
                    public void onResponse(Call<DeleteResponse> call, Response<DeleteResponse> response) {
                        if(response.isSuccessful()){
                            Toast.makeText(getActivity(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                            listStudent.remove(i);
                            adapter.notifyDataSetChanged();
                        }else {
                            Toast.makeText(getActivity(), "Fail to delete", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<DeleteResponse> call, Throwable t) {
                        Toast.makeText(getActivity(), "Fail to delete", Toast.LENGTH_SHORT).show();
                    }
                });
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    @OnClick(R.id.btn_add_new)
    public void createStudent(){
        Intent intent = new Intent(getActivity(),StudentFormActivity.class);
        intent.putExtra("TYPE","NEW");
        startActivity(intent);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void showDetail(int i) {
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        @SuppressLint("InflateParams") View dialogView = inflater.inflate(R.layout.student_detail, null);
        alertDialog.setView(dialogView);

        ImageView btnClose = dialogView.findViewById(R.id.btn_close);
        TextView tvName = dialogView.findViewById(R.id.tv_name);
        TextView tvGender = dialogView.findViewById(R.id.tv_gender);
        TextView tvPhone = dialogView.findViewById(R.id.tv_phone);
        TextView tvAddress = dialogView.findViewById(R.id.tv_address);

        tvName.setText(listStudent.get(i).getFirstname() +" "+ listStudent.get(i).getLastname());
        tvGender.setText(listStudent.get(i).getGender());
        tvPhone.setText(listStudent.get(i).getTel());
        tvAddress.setText(listStudent.get(i).getAddress());

        final AlertDialog dialog = alertDialog.create();
        Objects.requireNonNull(dialog.getWindow()).getAttributes().windowAnimations = R.style.DialogAnimation;
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    public void editStudent(int i) {
        Intent intent = new Intent(getActivity(),StudentFormActivity.class);
        intent.putExtra("TYPE","update");
        intent.putExtra("STUDENT",listStudent.get(i));
        startActivity(intent);
    }
}
