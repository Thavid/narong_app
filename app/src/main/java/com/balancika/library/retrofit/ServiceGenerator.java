package com.balancika.library.retrofit;

import com.balancika.library.util.Constant;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class ServiceGenerator {
    public static String API_BASE_URL = Constant.BASE_URL;

    private static OkHttpClient.Builder httpClient = new OkHttpClient.Builder()
                                                        .readTimeout(120, TimeUnit.SECONDS)
                                                        .connectTimeout(120, TimeUnit.SECONDS);

    private static Retrofit.Builder builder = new Retrofit.Builder()
                    .baseUrl(API_BASE_URL)
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create());

    private static Retrofit retrofit = builder.build();

    /*public static <S> S createService(Class<S> serviceClass) {
        if (!TextUtils.isEmpty(Constant.HEADER)) {
            AuthenticationInterceptor interceptor = new AuthenticationInterceptor(Constant.HEADER);
            if (!httpClient.interceptors().contains(interceptor)) {
                httpClient.addInterceptor(interceptor);

                builder.client(httpClient.build());
                retrofit = builder.build();
            }
        }
        return retrofit.create(serviceClass);
    }*/


   public static <S> S createService(Class<S> serviceClass) {
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request original = chain.request();

                Request.Builder requestBuilder = original.newBuilder()
                        .header("Accept", "application/json");
                Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        });
    return retrofit.create(serviceClass);
    }
}
