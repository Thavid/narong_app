package com.balancika.library.retrofit;

import com.balancika.library.util.Constant;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class AuthenticationInterceptor implements Interceptor {
    private String authToken;
    public AuthenticationInterceptor(String token) {
        this.authToken = token;
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request original = chain.request();
        Request.Builder builder = original.newBuilder()
                .header("Authorization", authToken)
                .header("x-profile", Constant.PROFILE)
                .header("Connection", "close");

        Request request = builder.build();
        return chain.proceed(request);
    }
}
