package com.balancika.library;


import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.balancika.library.adapter.BookAdapter;
import com.balancika.library.api.Api;
import com.balancika.library.request.AddBorrow;
import com.balancika.library.request.DetailsEntity;
import com.balancika.library.response.AddBookResponse;
import com.balancika.library.response.AddBorrowResponse;
import com.balancika.library.response.DataEntity;
import com.balancika.library.response.DeleteBookResponse;
import com.balancika.library.response.DeleteResponse;
import com.balancika.library.response.ListBookResponse;
import com.balancika.library.response.StudentData;
import com.balancika.library.retrofit.ServiceGenerator;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;


/**
 * A simple {@link Fragment} subclass.
 */
public class ListBookFragment extends Fragment {
    @BindView(R.id.rv_book)
    RecyclerView rvBook;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    BookAdapter bookAdapter;
    List<DataEntity> listBook = new ArrayList<>();
    final Calendar myCalendar = Calendar.getInstance();
    private TextView edDate;
    private int studentId;
    private String studentName;
    private Button btnSelectStu;
    private StudentData studentData;

    public ListBookFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_list_book, container, false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this,view);

        bookAdapter = new BookAdapter(this,listBook);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(),LinearLayoutManager.VERTICAL,false);
        rvBook.setLayoutManager(layoutManager);
        rvBook.setAdapter(bookAdapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        listBook.clear();
        progressBar.setVisibility(View.VISIBLE);
        Api.BookApi api = ServiceGenerator.createService(Api.BookApi.class);
        Call<ListBookResponse> call = api.getBook("");
        call.enqueue(new Callback<ListBookResponse>() {
            @Override
            public void onResponse(Call<ListBookResponse> call, Response<ListBookResponse> response) {
                listBook.addAll(response.body().getData());
                bookAdapter.notifyDataSetChanged();
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<ListBookResponse> call, Throwable t) {
                Toast.makeText(getActivity(), "Loaded data failed", Toast.LENGTH_SHORT).show();
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    @OnClick(R.id.btn_add_book)
    public void addBook(){
        Intent intent = new Intent(getActivity(),AddBookFormActivity.class);
        intent.putExtra("TYPE","new");
        startActivity(intent);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void showBorrowDialog(final int i){
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        @SuppressLint("InflateParams") View dialogView = inflater.inflate(R.layout.borrow_dialog, null);
        alertDialog.setView(dialogView);

        ImageView btnClose = dialogView.findViewById(R.id.btn_close);
        TextView tvTitle = dialogView.findViewById(R.id.tv_book_title);
        btnSelectStu = dialogView.findViewById(R.id.btn_student);
        edDate = dialogView.findViewById(R.id.ed_date_picker);
        final TextView tvCurrentData = dialogView.findViewById(R.id.tv_current_date);
        Button btnConfirm = dialogView.findViewById(R.id.btn_confirm);

        tvTitle.setText(listBook.get(i).getTitle());

        final AlertDialog dialog = alertDialog.create();
        Objects.requireNonNull(dialog.getWindow()).getAttributes().windowAnimations = R.style.DialogAnimation;
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddBorrow addBorrow = new AddBorrow();
                DetailsEntity detailsEntity = new DetailsEntity();
                List<DetailsEntity> listDetailEntity = new ArrayList<>();
                addBorrow.setAmount(1);
                addBorrow.setBookDate(tvCurrentData.getText().toString());
                addBorrow.setBookReturn(edDate.getText().toString());
                addBorrow.setStudent(studentId);
                addBorrow.setUser("vid");
                detailsEntity.setAmount(1);
                detailsEntity.setBook(listBook.get(i).getId());
                listDetailEntity.add(detailsEntity);
                addBorrow.setDetails(listDetailEntity);
                Api.BookApi api = ServiceGenerator.createService(Api.BookApi.class);
                Call<AddBorrowResponse> call = api.addBorrow(addBorrow);
                call.enqueue(new Callback<AddBorrowResponse>() {
                    @Override
                    public void onResponse(Call<AddBorrowResponse> call, Response<AddBorrowResponse> response) {
                        if(response.isSuccessful())
                            Toast.makeText(getActivity(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        else
                            Toast.makeText(getActivity(), "Add Failed", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onFailure(Call<AddBorrowResponse> call, Throwable t) {
                        Toast.makeText(getActivity(), "Add Failed", Toast.LENGTH_SHORT).show();
                    }

                });
                dialog.dismiss();
            }
        });
        btnSelectStu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(),SearchActivity.class);
                intent.putExtra("type","selectStudent");
                startActivityForResult(intent,1);
            }
        });
        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                String myFormat = "dd/MM/yyyy"; //In which you need put here
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                edDate.setText(sdf.format(myCalendar.getTime()));
            }
        };

        edDate.setOnClickListener(new View.OnClickListener() {
              @Override
              public void onClick(View v) {
                  // TODO Auto-generated method stub
                  new DatePickerDialog(getActivity(), date, myCalendar
                          .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                          myCalendar.get(Calendar.DAY_OF_MONTH)).show();
              }
          });

        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        tvCurrentData.setText(df.format(c));
        dialog.show();
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void showDeleteDialog(final int i) {
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        @SuppressLint("InflateParams") View dialogView = inflater.inflate(R.layout.confirm_dialog, null);
        alertDialog.setView(dialogView);

        TextView tvTitle = dialogView.findViewById(R.id.tv_title);
        TextView tvDescription = dialogView.findViewById(R.id.tv_description);
        Button btnCancel = dialogView.findViewById(R.id.btn_done);
        Button btnConfirm = dialogView.findViewById(R.id.btn_confirm);

        tvTitle.setText("Confirm");
        tvDescription.setText("Are you sure to delete?");
        btnConfirm.setText("Delete");

        final AlertDialog dialog = alertDialog.create();
        Objects.requireNonNull(dialog.getWindow()).getAttributes().windowAnimations = R.style.DialogAnimation;
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //callback.confirm();
                Api.BookApi api = ServiceGenerator.createService(Api.BookApi.class);
                Call<DeleteBookResponse> call = api.deleteBook(listBook.get(i).getId());;
                call.enqueue(new Callback<DeleteBookResponse>() {
                    @Override
                    public void onResponse(Call<DeleteBookResponse> call, Response<DeleteBookResponse> response) {
                        if(response.isSuccessful()){
                            Toast.makeText(getActivity(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                            listBook.remove(i);
                            bookAdapter.notifyDataSetChanged();
                        }else {
                            Toast.makeText(getActivity(), "Fail to delete", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<DeleteBookResponse> call, Throwable t) {
                        Toast.makeText(getActivity(), "Fail to delete", Toast.LENGTH_SHORT).show();
                    }
                });
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    public void updateBook(int i) {
        Intent intent = new Intent(getActivity(),AddBookFormActivity.class);
        intent.putExtra("TYPE","update");
        intent.putExtra("BOOKDATA",listBook.get(i));
        startActivity(intent);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 1){
            if(resultCode == RESULT_OK){
                studentData = (StudentData) data.getSerializableExtra("STUDATA");
                studentId = studentData.getId();
                btnSelectStu.setText(studentData.getFirstname()+" "+studentData.getLastname());
            }
        }
    }
}
