package com.balancika.library.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AddBookRequest {

    @Expose
    @SerializedName("title")
    private String title;
    @Expose
    @SerializedName("id")
    private int id;
    @Expose
    @SerializedName("series")
    private String series;
    @Expose
    @SerializedName("release")
    private String release;
    @Expose
    @SerializedName("genre")
    private String genre;
    @Expose
    @SerializedName("author")
    private String author;

    public AddBookRequest(String title, String release, String genre, String author) {
        this.title = title;
        this.release = release;
        this.genre = genre;
        this.author = author;
    }

    public AddBookRequest(String title, int id, String release, String genre, String author) {
        this.title = title;
        this.id = id;
        this.release = release;
        this.genre = genre;
        this.author = author;
    }
}
