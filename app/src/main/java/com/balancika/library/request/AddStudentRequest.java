package com.balancika.library.request;

import com.google.gson.annotations.SerializedName;

public class AddStudentRequest {

    @SerializedName("address")
    public String address;
    @SerializedName("firstname")
    public String firstname;
    @SerializedName("gender")
    public String gender;
    @SerializedName("lastname")
    public String lastname;
    @SerializedName("tel")
    public String tel;

    public AddStudentRequest(String address, String firstname, String gender, String lastname, String tel) {
        this.address = address;
        this.firstname = firstname;
        this.gender = gender;
        this.lastname = lastname;
        this.tel = tel;
    }

    public String getAddress() {
        return address;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getGender() {
        return gender;
    }

    public String getLastname() {
        return lastname;
    }

    public String getTel() {
        return tel;
    }
}
