package com.balancika.library.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DetailsEntity {
    @Expose
    @SerializedName("book")
    private int book;
    @Expose
    @SerializedName("amount")
    private int amount;

    public void setBook(int book) {
        this.book = book;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }
}
