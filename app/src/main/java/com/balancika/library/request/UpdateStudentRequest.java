package com.balancika.library.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UpdateStudentRequest {
    @Expose
    @SerializedName("tel")
    private String Tel;
    @Expose
    @SerializedName("lastname")
    private String Lastname;
    @Expose
    @SerializedName("id")
    private int Id;
    @Expose
    @SerializedName("gender")
    private String Gender;
    @Expose
    @SerializedName("firstname")
    private String Firstname;
    @Expose
    @SerializedName("address")
    private String Address;

    public UpdateStudentRequest(String tel, String lastname, int id, String gender, String firstname, String address) {
        Tel = tel;
        Lastname = lastname;
        Id = id;
        Gender = gender;
        Firstname = firstname;
        Address = address;
    }
}
