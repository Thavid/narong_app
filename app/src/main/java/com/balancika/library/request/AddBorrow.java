package com.balancika.library.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AddBorrow {

    @Expose
    @SerializedName("user")
    private String user;
    @Expose
    @SerializedName("student")
    private int student;
    @Expose
    @SerializedName("details")
    private List<DetailsEntity> details;
    @Expose
    @SerializedName("bookReturn")
    private String bookReturn;
    @Expose
    @SerializedName("bookDate")
    private String bookDate;
    @Expose
    @SerializedName("amount")
    private int amount;

    public void setUser(String user) {
        this.user = user;
    }

    public void setStudent(int student) {
        this.student = student;
    }

    public void setDetails(List<DetailsEntity> details) {
        this.details = details;
    }

    public void setBookReturn(String bookReturn) {
        this.bookReturn = bookReturn;
    }

    public void setBookDate(String bookDate) {
        this.bookDate = bookDate;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }
}
