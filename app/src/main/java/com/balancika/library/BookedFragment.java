package com.balancika.library;


import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.balancika.library.adapter.BorrowAdapter;
import com.balancika.library.api.Api;
import com.balancika.library.response.BookingResponse;
import com.balancika.library.response.DataItem;
import com.balancika.library.response.DeleteBookedResponse;
import com.balancika.library.response.ReturnBookedResponse;
import com.balancika.library.retrofit.ServiceGenerator;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class BookedFragment extends Fragment {
    @BindView(R.id.rv_borrow)
    RecyclerView recyclerView;
    @BindView(R.id.no_data)
    TextView noData;
    BorrowAdapter adapter;
    private  Api.StudentApi api;
    private DrawerLayout mDrawerLayout;
    private List<DataItem> dataItemList = new ArrayList<>();
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_booked, container, false);
        for (ViewParent parent = container.getParent(); parent != null; parent = parent.getParent()) {

            if (parent instanceof DrawerLayout) {

                mDrawerLayout = (DrawerLayout) parent;
                mDrawerLayout.setDrawerListener(new DrawerLayout.SimpleDrawerListener() {

                    @Override
                    public void onDrawerOpened(View drawerView) {
                        dataItemList.clear();
                        adapter = new BorrowAdapter(dataItemList,BookedFragment.this);
                        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(),LinearLayoutManager.VERTICAL,false);
                        recyclerView.setLayoutManager(layoutManager);
                        recyclerView.setAdapter(adapter);
                        api = ServiceGenerator.createService(Api.StudentApi.class);
                        Call<BookingResponse> call = api.getBooking();
                        call.enqueue(new Callback<BookingResponse>() {
                            @Override
                            public void onResponse(Call<BookingResponse> call, Response<BookingResponse> response) {
                                if(response.isSuccessful()){
                                    if(response.body().getData().size() != 0){
                                        dataItemList.addAll(response.body().getData());
                                        adapter.notifyDataSetChanged();
                                    }
                                    if(dataItemList.size() != 0){
                                        noData.setVisibility(View.GONE);
                                    }else{
                                        noData.setVisibility(View.VISIBLE);
                                    }
                                }
                            }

                            @Override
                            public void onFailure(Call<BookingResponse> call, Throwable t) {

                            }
                        });
                    }

                    @Override
                    public void onDrawerClosed(View drawerView) {
                    }

                });
                break;
            }
        }

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this,view);
    }

    @Override
    public void onResume() {
        super.onResume();
        /*dataItemList.clear();
        adapter = new BorrowAdapter(dataItemList,BookedFragment.this);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(),LinearLayoutManager.VERTICAL,false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
        api = ServiceGenerator.createService(Api.StudentApi.class);
        Call<BookingResponse> call = api.getBooking();
        call.enqueue(new Callback<BookingResponse>() {
            @Override
            public void onResponse(Call<BookingResponse> call, Response<BookingResponse> response) {
                if(response.isSuccessful()){
                    if(response.body().getData().size() != 0){
                        dataItemList.addAll(response.body().getData());
                        adapter.notifyDataSetChanged();
                    }
                    if(dataItemList.size() != 0){
                        noData.setVisibility(View.GONE);
                    }else{
                        noData.setVisibility(View.VISIBLE);
                    }
                }
            }

            @Override
            public void onFailure(Call<BookingResponse> call, Throwable t) {

            }
        });*/
    }

    public void refreshFragment(){

    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void deleteBooked(final int id){
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        @SuppressLint("InflateParams")
        final View dialogView = inflater.inflate(R.layout.confirm_dialog, null);
        alertDialog.setView(dialogView);
        final AlertDialog dialog = alertDialog.create();
        Objects.requireNonNull(dialog.getWindow()).getAttributes().windowAnimations = R.style.DialogAnimation;

        TextView title = dialogView.findViewById(R.id.tv_title);
        title.setVisibility(View.GONE);
        TextView message = dialogView.findViewById(R.id.tv_description);
        message.setText("Are you sure you want to delete??");
        Button no = dialogView.findViewById(R.id.btn_done);
        no.setText("No");
        Button ok = dialogView.findViewById(R.id.btn_confirm);
        ok.setText("Yes");
        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Call<DeleteBookedResponse> call = api.deleteBooked(id);
                call.enqueue(new Callback<DeleteBookedResponse>() {
                    @Override
                    public void onResponse(Call<DeleteBookedResponse> call, Response<DeleteBookedResponse> response) {
                        if(response.isSuccessful()){
                            if(response.body().getStatus().equals("SUCCESS")){
                                dialog.dismiss();
                                for(int i = 0 ; i < dataItemList.size();i++){
                                    if(dataItemList.get(i).getId() == id){
                                        dataItemList.remove(i);
                                        adapter.notifyDataSetChanged();
                                    }
                                }
                                if(dataItemList.size() != 0){
                                    noData.setVisibility(View.GONE);
                                }else{
                                    noData.setVisibility(View.VISIBLE);
                                }
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<DeleteBookedResponse> call, Throwable t) {

                    }
                });
            }
        });
        dialog.show();
    }
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void returnBook(final int id){
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        @SuppressLint("InflateParams")
        final View dialogView = inflater.inflate(R.layout.confirm_dialog, null);
        alertDialog.setView(dialogView);
        final AlertDialog dialog = alertDialog.create();
        Objects.requireNonNull(dialog.getWindow()).getAttributes().windowAnimations = R.style.DialogAnimation;

        TextView title = dialogView.findViewById(R.id.tv_title);
        title.setVisibility(View.GONE);
        TextView message = dialogView.findViewById(R.id.tv_description);
        message.setText("Are you sure you want to return book?");
        Button no = dialogView.findViewById(R.id.btn_done);
        no.setText("No");
        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        Button ok = dialogView.findViewById(R.id.btn_confirm);
        ok.setText("Yes");
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Call<ReturnBookedResponse> call = api.returnBooked(id);
                call.enqueue(new Callback<ReturnBookedResponse>() {
                    @Override
                    public void onResponse(Call<ReturnBookedResponse> call, Response<ReturnBookedResponse> response) {
                        if(response.isSuccessful()){
                            if(response.body().getStatus().equals("SUCCESS")){
                                dialog.dismiss();
                                for(int i = 0 ; i < dataItemList.size();i++){
                                    if(dataItemList.get(i).getId() == id){
                                        dataItemList.remove(i);
                                        adapter.notifyDataSetChanged();
                                    }
                                }
                                if(dataItemList.size() != 0){
                                    noData.setVisibility(View.GONE);
                                }else{
                                    noData.setVisibility(View.VISIBLE);
                                }
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ReturnBookedResponse> call, Throwable t) {

                    }
                });
            }
        });
        dialog.show();
    }
}
