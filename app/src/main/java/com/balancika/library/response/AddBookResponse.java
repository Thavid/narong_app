package com.balancika.library.response;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
public class AddBookResponse {
    @Expose
    @SerializedName("data")
    private DataEntity data;
    @Expose
    @SerializedName("statusCode")
    private int statusCode;
    @Expose
    @SerializedName("status")
    private String status;
    @Expose
    @SerializedName("message")
    private String message;

    public DataEntity getData() {
        return data;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public String getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }
}
