package com.balancika.library.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class DataEntity implements Serializable {
    @Expose
    @SerializedName("release")
    private String release;
    @Expose
    @SerializedName("series")
    private String series;
    @Expose
    @SerializedName("author")
    private String author;
    @Expose
    @SerializedName("title")
    private String title;
    @Expose
    @SerializedName("genre")
    private String genre;
    @Expose
    @SerializedName("id")
    private int id;

    public String getRelease() {
        return release;
    }

    public String getSeries() {
        return series;
    }

    public String getAuthor() {
        return author;
    }

    public String getTitle() {
        return title;
    }

    public String getGenre() {
        return genre;
    }

    public int getId() {
        return id;
    }
}
