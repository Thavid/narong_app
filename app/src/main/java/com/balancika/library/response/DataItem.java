package com.balancika.library.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DataItem{

	@SerializedName("bookReturn")
	private String bookReturn;

	@SerializedName("amount")
	private int amount;

	@SerializedName("bookDate")
	private String bookDate;

	@SerializedName("student")
	private Student student;

	@SerializedName("details")
	private List<DetailsItem> details;

	@SerializedName("id")
	private int id;

	@SerializedName("user")
	private String user;

	public void setBookReturn(String bookReturn){
		this.bookReturn = bookReturn;
	}

	public String getBookReturn(){
		return bookReturn;
	}

	public void setAmount(int amount){
		this.amount = amount;
	}

	public int getAmount(){
		return amount;
	}

	public void setBookDate(String bookDate){
		this.bookDate = bookDate;
	}

	public String getBookDate(){
		return bookDate;
	}

	public void setStudent(Student student){
		this.student = student;
	}

	public Student getStudent(){
		return student;
	}

	public void setDetails(List<DetailsItem> details){
		this.details = details;
	}

	public List<DetailsItem> getDetails(){
		return details;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setUser(String user){
		this.user = user;
	}

	public String getUser(){
		return user;
	}

	@Override
 	public String toString(){
		return 
			"DataItem{" + 
			"bookReturn = '" + bookReturn + '\'' + 
			",amount = '" + amount + '\'' + 
			",bookDate = '" + bookDate + '\'' + 
			",student = '" + student + '\'' + 
			",details = '" + details + '\'' + 
			",id = '" + id + '\'' + 
			",user = '" + user + '\'' + 
			"}";
		}
}