package com.balancika.library.response;

import com.google.gson.annotations.SerializedName;

public class DeleteBookedResponse{

	@SerializedName("data")
	private int data;

	@SerializedName("message")
	private String message;

	@SerializedName("status")
	private String status;

	@SerializedName("statusCode")
	private int statusCode;

	public void setData(int data){
		this.data = data;
	}

	public int getData(){
		return data;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	public void setStatusCode(int statusCode){
		this.statusCode = statusCode;
	}

	public int getStatusCode(){
		return statusCode;
	}
}