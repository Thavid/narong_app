package com.balancika.library.response;

import com.google.gson.annotations.SerializedName;

public class Book{

	@SerializedName("author")
	private String author;

	@SerializedName("series")
	private String series;

	@SerializedName("release")
	private String release;

	@SerializedName("genre")
	private String genre;

	@SerializedName("id")
	private int id;

	@SerializedName("title")
	private String title;

	public void setAuthor(String author){
		this.author = author;
	}

	public String getAuthor(){
		return author;
	}

	public void setSeries(String series){
		this.series = series;
	}

	public String getSeries(){
		return series;
	}

	public void setRelease(String release){
		this.release = release;
	}

	public String getRelease(){
		return release;
	}

	public void setGenre(String genre){
		this.genre = genre;
	}

	public String getGenre(){
		return genre;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setTitle(String title){
		this.title = title;
	}

	public String getTitle(){
		return title;
	}

	@Override
 	public String toString(){
		return 
			"Book{" + 
			"author = '" + author + '\'' + 
			",series = '" + series + '\'' + 
			",release = '" + release + '\'' + 
			",genre = '" + genre + '\'' + 
			",id = '" + id + '\'' + 
			",title = '" + title + '\'' + 
			"}";
		}
}