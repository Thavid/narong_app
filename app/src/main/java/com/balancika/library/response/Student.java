package com.balancika.library.response;


import com.google.gson.annotations.SerializedName;

public class Student{

	@SerializedName("firstname")
	private String firstname;

	@SerializedName("address")
	private String address;

	@SerializedName("gender")
	private String gender;

	@SerializedName("tel")
	private String tel;

	@SerializedName("id")
	private int id;

	@SerializedName("lastname")
	private String lastname;

	public void setFirstname(String firstname){
		this.firstname = firstname;
	}

	public String getFirstname(){
		return firstname;
	}

	public void setAddress(String address){
		this.address = address;
	}

	public String getAddress(){
		return address;
	}

	public void setGender(String gender){
		this.gender = gender;
	}

	public String getGender(){
		return gender;
	}

	public void setTel(String tel){
		this.tel = tel;
	}

	public String getTel(){
		return tel;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setLastname(String lastname){
		this.lastname = lastname;
	}

	public String getLastname(){
		return lastname;
	}

	@Override
 	public String toString(){
		return 
			"Student{" + 
			"firstname = '" + firstname + '\'' + 
			",address = '" + address + '\'' + 
			",gender = '" + gender + '\'' + 
			",tel = '" + tel + '\'' + 
			",id = '" + id + '\'' + 
			",lastname = '" + lastname + '\'' + 
			"}";
		}
}