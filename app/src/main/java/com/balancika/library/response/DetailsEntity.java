package com.balancika.library.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DetailsEntity {
    @Expose
    @SerializedName("amount")
    private int amount;
    @Expose
    @SerializedName("book")
    private int book;
    @Expose
    @SerializedName("id")
    private int id;
}
