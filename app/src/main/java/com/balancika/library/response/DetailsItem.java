package com.balancika.library.response;

import com.google.gson.annotations.SerializedName;

public class DetailsItem{

	@SerializedName("amount")
	private int amount;

	@SerializedName("book")
	private Book book;

	@SerializedName("id")
	private int id;

	public void setAmount(int amount){
		this.amount = amount;
	}

	public int getAmount(){
		return amount;
	}

	public void setBook(Book book){
		this.book = book;
	}

	public Book getBook(){
		return book;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	@Override
 	public String toString(){
		return 
			"DetailsItem{" + 
			"amount = '" + amount + '\'' + 
			",book = '" + book + '\'' + 
			",id = '" + id + '\'' + 
			"}";
		}
}