package com.balancika.library.response;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class StudentData implements Serializable {
    @SerializedName("id")
    public int id;
    @SerializedName("tel")
    public String tel;
    @SerializedName("gender")
    public String gender;
    @SerializedName("address")
    public String address;
    @SerializedName("lastname")
    public String lastname;
    @SerializedName("firstname")
    public String firstname;

    public int getId() {
        return id;
    }

    public String getTel() {
        return tel;
    }

    public String getGender() {
        return gender;
    }

    public String getAddress() {
        return address;
    }

    public String getLastname() {
        return lastname;
    }

    public String getFirstname() {
        return firstname;
    }
}
