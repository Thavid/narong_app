package com.balancika.library.response;

import com.google.gson.annotations.SerializedName;

public class AddStudentResponse {


    @SerializedName("message")
    public String message;
    @SerializedName("status")
    public String status;
    @SerializedName("statusCode")
    public int statusCode;
    @SerializedName("data")
    public Data data;

    public String getMessage() {
        return message;
    }

    public String getStatus() {
        return status;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public Data getData() {
        return data;
    }
}
