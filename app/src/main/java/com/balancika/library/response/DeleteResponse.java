package com.balancika.library.response;

import com.google.gson.annotations.SerializedName;

public class DeleteResponse {

    @SerializedName("message")
    public String message;
    @SerializedName("status")
    public String status;
    @SerializedName("statusCode")
    public int statusCode;
    @SerializedName("data")
    public int data;

    public String getMessage() {
        return message;
    }

    public String getStatus() {
        return status;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public int getData() {
        return data;
    }
}
