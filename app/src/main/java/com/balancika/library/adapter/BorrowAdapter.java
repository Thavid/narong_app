package com.balancika.library.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.balancika.library.BookedFragment;
import com.balancika.library.R;
import com.balancika.library.response.DataItem;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BorrowAdapter extends RecyclerView.Adapter<BorrowAdapter.ViewHolder> {

    private List<DataItem> dataItemList;
    private BookedFragment bookedFragment;

    public BorrowAdapter(List<DataItem> dataItemList,BookedFragment bookedFragment) {
        this.dataItemList = dataItemList;
        this.bookedFragment = bookedFragment;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new ViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.cu_borrow, viewGroup, false));
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        final DataItem item = dataItemList.get(i);
        viewHolder.bookDate.setText(item.getBookDate());
        viewHolder.bookReturn.setText(item.getBookReturn());
        viewHolder.stuName.setText(item.getStudent().getFirstname().equals("")?"N/A":item.getStudent().getFirstname()+" "+item.getStudent().getLastname());
        viewHolder.stuTel.setText(item.getStudent().getTel());
        viewHolder.bookTitle.setText(item.getDetails().get(0).getBook().getTitle());
        viewHolder.delete.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onClick(View v) {
                bookedFragment.deleteBooked(item.getDetails().get(0).getId());
            }
        });

        viewHolder.btnReturn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bookedFragment.returnBook(item.getDetails().get(0).getId());
            }
        });
    }

    @Override
    public int getItemCount() {
        return dataItemList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.student_name)
        TextView stuName;
        @BindView(R.id.stu_tel)
        TextView stuTel;
        @BindView(R.id.book_title)
        TextView bookTitle;
        @BindView(R.id.bookDate)
        TextView bookDate;
        @BindView(R.id.bookReturn)
        TextView bookReturn;
        @BindView(R.id.delete_booked)
        ImageView delete;
        @BindView(R.id.btn_return)
        Button btnReturn;
        ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
