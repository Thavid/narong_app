package com.balancika.library.adapter;

import android.content.Context;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.balancika.library.R;
import com.balancika.library.StudentFragment;
import com.balancika.library.response.StudentData;
import com.balancika.library.response.StudentResponse;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class StudentAdapter extends RecyclerView.Adapter<StudentAdapter.ViewHolder> {
    private Context context;
    private List<StudentData> listData;
    private StudentFragment studentFragment;

    public StudentAdapter(Context context, List<StudentData> listData, StudentFragment studentFragment) {
        this.context = context;
        this.listData = listData;
        this.studentFragment = studentFragment;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return  new ViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.st_item_layout,viewGroup,false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {
        viewHolder.tvName.setText(listData.get(i).getFirstname() +" "+listData.get(i).lastname);
        viewHolder.btnDelete.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onClick(View v) {
                studentFragment.showDeleteDialog(i);
            }
        });
        viewHolder.btn_detail.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onClick(View view) {
                studentFragment.showDetail(i);
            }
        });

        viewHolder.btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                studentFragment.editStudent(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return listData.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.btn_delete_stu)
        ImageView btnDelete;
        @BindView(R.id.stu_img)
        ImageView btn_detail;
        @BindView(R.id.stu_name)
        TextView tvName;
        @BindView(R.id.btn_edit_stu)
        ImageView btnEdit;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
}
