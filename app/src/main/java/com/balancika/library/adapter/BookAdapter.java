package com.balancika.library.adapter;

import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.balancika.library.ListBookFragment;
import com.balancika.library.R;
import com.balancika.library.response.DataEntity;


import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BookAdapter extends RecyclerView.Adapter<BookAdapter.ViewHolder> {
    private ListBookFragment listBookFragment;
    private List<DataEntity> listBook = new ArrayList<>();

    public BookAdapter(ListBookFragment listBookFragment, List<DataEntity> listBook) {
        this.listBookFragment = listBookFragment;
        this.listBook = listBook;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new ViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.book_item,viewGroup,false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {
        viewHolder.tvTitle.setText(listBook.get(i).getTitle());
        viewHolder.tvType.setText("Type: "+listBook.get(i).getGenre());
        viewHolder.tvAuthor.setText("Author: "+listBook.get(i).getAuthor());
        viewHolder.tvRelease.setText("Release: "+listBook.get(i).getRelease());

        viewHolder.btnBorrow.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onClick(View v) {
                listBookFragment.showBorrowDialog(i);
            }
        });

        viewHolder.btnDelete.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onClick(View v) {
                listBookFragment.showDeleteDialog(i);
            }
        });

        viewHolder.btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listBookFragment.updateBook(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return listBook.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.btn_borrow)
        TextView btnBorrow;
        @BindView(R.id.btn_delete)
        LinearLayout btnDelete;
        @BindView(R.id.btn_edit)
        LinearLayout btnEdit;
        @BindView(R.id.tv_book_title)
        TextView tvTitle;
        @BindView(R.id.tv_type)
        TextView tvType;
        @BindView(R.id.tv_author)
        TextView tvAuthor;
        @BindView(R.id.tv_release)
        TextView tvRelease;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
}
