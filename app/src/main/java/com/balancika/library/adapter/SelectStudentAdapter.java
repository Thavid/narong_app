package com.balancika.library.adapter;

import android.content.Context;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.balancika.library.R;
import com.balancika.library.SearchActivity;
import com.balancika.library.StudentFragment;
import com.balancika.library.response.StudentData;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SelectStudentAdapter extends RecyclerView.Adapter<SelectStudentAdapter.ViewHolder> {
    private Context context;
    private List<StudentData> listData;
    private SearchActivity searchActivity;

    public SelectStudentAdapter(Context context, List<StudentData> listData) {
        this.context = context;
        this.listData = listData;
        searchActivity = (SearchActivity) context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return  new SelectStudentAdapter.ViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.cu_select_stu,viewGroup,false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {
        viewHolder.tvName.setText("Name: "+listData.get(i).getFirstname() +" "+listData.get(i).lastname);
        viewHolder.tvGender.setText("Gender: "+listData.get(i).getGender());
        viewHolder.tvPhone.setText("Phone: "+listData.get(i).getTel());
    }

    @Override
    public int getItemCount() {
        return listData.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.tv_name)
        TextView tvName;
        @BindView(R.id.tv_gender)
        TextView tvGender;
        @BindView(R.id.tv_phone)
        TextView tvPhone;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    searchActivity.getStudent(getAdapterPosition());
                }
            });
        }
    }
}
