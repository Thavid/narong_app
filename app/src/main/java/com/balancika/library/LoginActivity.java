package com.balancika.library;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.balancika.library.api.Api;
import com.balancika.library.response.LoginResponse;
import com.balancika.library.response.SignUp;
import com.balancika.library.response.SignUpResponse;
import com.balancika.library.retrofit.ServiceGenerator;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity  implements Validator.ValidationListener  {

    @NotEmpty
    @BindView(R.id.username)
    EditText username;
    @NotEmpty
    @BindView(R.id.password)
    EditText password;
    private SignUp signUp = new SignUp();
    private Validator validator;
    private UserManager userManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        userManager = UserManager.getInstance(getSharedPreferences("USER",Context.MODE_PRIVATE));
        if(userManager.getUser() != null){
            startActivity(new Intent(LoginActivity.this,MainActivity.class));
        }
        validator =  new Validator(this);
        validator.setValidationListener(LoginActivity.this);
    }

    @OnClick(R.id.btn_login)
    public void login(){
        validator.validate();
    }

    @OnClick(R.id.textView14)
    public void goToSignUp(){
        startActivity(new Intent(this,SignUpActivity.class));
    }

    @Override
    public void onValidationSucceeded() {
        signUp.setUsername(username.getText().toString());
        signUp.setPassword(password.getText().toString());
        Api.StudentApi api = ServiceGenerator.createService(Api.StudentApi.class);
        Call<LoginResponse> call = api.login(signUp);
        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                if(response.isSuccessful()){
                    Intent intent = new Intent(LoginActivity.this,MainActivity.class);
                    intent.putExtra("NAME",signUp.getUsername());
                    startActivity(intent);
                    userManager.saveUser(signUp.getUsername());
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                Toast.makeText(LoginActivity.this, "Login Failed", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }
    }
}
