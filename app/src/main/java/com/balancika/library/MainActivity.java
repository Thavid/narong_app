package com.balancika.library;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.balancika.library.adapter.StudentAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;
    @BindView(R.id.tool_bar)
    Toolbar toolbar;
    @BindView(R.id.navigation)
    NavigationView navigationView;
    @BindView(R.id.toolbar_title)
    TextView toolBarTitle;
    private FragmentManager fm;
    private Fragment studentFragment = new StudentFragment();
    private Fragment listBookFragment = new ListBookFragment();
    private Fragment bookedFragment = new BookedFragment();
    private Fragment profileFragment = new ProfileFragment();
    private int lastPosition;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);

        toolBarTitle.setText("List Students");
        fm = getSupportFragmentManager();
        navigationView.setCheckedItem(R.id.task);
        fm.beginTransaction().add(R.id.container, profileFragment, "4").show(profileFragment).commit();
        fm.beginTransaction().add(R.id.container, bookedFragment, "3").show(bookedFragment).commit();
        fm.beginTransaction().add(R.id.container, listBookFragment, "2").show(listBookFragment).commit();
        fm.beginTransaction().add(R.id.container, studentFragment, "1").show(studentFragment).commit();
        setUpDrawer();
    }

    private void setUpDrawer() {
        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.open, R.string.close);
        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);
       /* final MenuItem defaultSelected = navigationView.getMenu().getItem(0);
        defaultSelected.setCheckable(true);*/
        toolbar.setNavigationIcon(R.drawable.ic_dehaze_black_24dp);

        View headerview = navigationView.getHeaderView(0);
        TextView tvUsername = headerview.findViewById(R.id.tv_username);
        tvUsername.setText(getIntent().getStringExtra("NAME"));
        final CircleImageView profile = headerview.findViewById(R.id.user_img);

      /*  btnViewProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navigationView.getMenu().getItem(1).setChecked(false);
                setNavigationViewItemSelected(navigationView.getMenu().getItem(0),true);
                toolBarTitle.setVisibility(View.VISIBLE);
                toolBarTitle.setText(R.string.my_profile);
                drawerLayout.closeDrawer(GravityCompat.START);
            }
        });*/

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        setNavigationViewItemSelected(menuItem,false);
        drawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }

    public void setNavigationViewItemSelected(MenuItem menuItem , boolean viewProfile){
        if(!viewProfile){
            switch (menuItem.getItemId()) {
                case R.id.task:
                    lastPosition = 0;
                    toolBarTitle.setText("List Students");
                    fm.beginTransaction().show(studentFragment).hide(listBookFragment).hide(bookedFragment).hide(profileFragment).commit();
                    break;
                case R.id.notification:
                    lastPosition = 1;
                    toolBarTitle.setText("List Books");
                    fm.beginTransaction().show(listBookFragment).hide(studentFragment).hide(bookedFragment).hide(profileFragment).commit();
                    break;
                case R.id.history:
                    lastPosition = 2;
                    toolBarTitle.setText("Booking");
                    fm.beginTransaction().show(bookedFragment).hide(listBookFragment).hide(studentFragment).hide(profileFragment).commit();
                    break;
            }
        }else {
            fm.beginTransaction().show(profileFragment).hide(listBookFragment).hide(studentFragment).hide(bookedFragment).commit();
        }
    }
}
