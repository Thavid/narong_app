package com.balancika.library.api;

import com.balancika.library.request.AddBookRequest;
import com.balancika.library.request.AddBorrow;
import com.balancika.library.request.AddStudentRequest;
import com.balancika.library.request.UpdateStudentRequest;
import com.balancika.library.response.AddBookResponse;
import com.balancika.library.response.AddBorrowResponse;
import com.balancika.library.response.AddStudentResponse;
import com.balancika.library.response.DeleteBookResponse;
import com.balancika.library.response.BookingResponse;
import com.balancika.library.response.DeleteBookedResponse;
import com.balancika.library.response.DeleteResponse;
import com.balancika.library.response.ListBookResponse;
import com.balancika.library.response.LoginResponse;
import com.balancika.library.response.ReturnBookedResponse;
import com.balancika.library.response.SignUp;
import com.balancika.library.response.SignUpResponse;
import com.balancika.library.response.StudentResponse;

import io.reactivex.Single;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Query;

public interface Api {
    public interface StudentApi{
        @GET("api/student")
        Call<StudentResponse> getStudent();
        @POST("api/student")
        Call<AddStudentResponse> addStudent(@Body AddStudentRequest addStudentRequest);
        @DELETE("api/student")
        Call<DeleteResponse> deleteStudent(@Query("id")int id);
        @PUT("api/student")
        Call<AddStudentResponse> updateStudent(@Body UpdateStudentRequest studentRequest);
        @POST("api/user")
        Call<SignUpResponse> signUp(@Body SignUp signUp);

        @POST("api/user/login")
        Call<LoginResponse> login(@Body SignUp signUp);

        @GET("api/booking")
        Call<BookingResponse> getBooking();

        @DELETE("api/booking")
        Call<DeleteBookedResponse> deleteBooked(@Query("id") int id);

        @PUT("api/booking/return")
        Call<ReturnBookedResponse> returnBooked(@Query("id") int id);
    }

    public interface BookApi{
        @GET("api/book")
        Call<ListBookResponse> getBook(@Query("query")String query);
        @DELETE("api/book")
        Call<DeleteBookResponse> deleteBook(@Query("id")int id);
        @POST("api/book")
        Call<AddBookResponse> addBook(@Body AddBookRequest addBookRequest);
        @PUT("api/book")
        Call<AddBookResponse> updateBook(@Body AddBookRequest addBookRequest);
        @POST("api/booking")
        Call<AddBorrowResponse> addBorrow(@Body AddBorrow addBorrow);
    }

}
