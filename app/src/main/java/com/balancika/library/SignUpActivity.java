package com.balancika.library;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.balancika.library.api.Api;
import com.balancika.library.response.SignUp;
import com.balancika.library.response.SignUpResponse;
import com.balancika.library.retrofit.ServiceGenerator;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignUpActivity extends AppCompatActivity implements Validator.ValidationListener {
    @NotEmpty
    @BindView(R.id.username)
    EditText username;
    @BindView(R.id.done)
    TextView done;
    @NotEmpty
    @BindView(R.id.password)
    EditText password;
    @BindView(R.id.btn_back)
    ImageView back;
    @BindView(R.id.btn_done)
    TextView doneToolbar;
    @BindView(R.id.toolbar_title)
    TextView title;
    private SignUp signUp = new SignUp();
    private Validator validator;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        ButterKnife.bind(this);
        title.setText("Sing Up");
        validator =  new Validator(this);
        validator.setValidationListener(SignUpActivity.this);
        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validator.validate();
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        doneToolbar.setVisibility(View.GONE);
    }

    @Override
    public void onValidationSucceeded() {
        signUp.setUsername(username.getText().toString());
        signUp.setPassword(password.getText().toString());
        Api.StudentApi api = ServiceGenerator.createService(Api.StudentApi.class);
        Call<SignUpResponse> call = api.signUp(signUp);
        call.enqueue(new Callback<SignUpResponse>() {
            @Override
            public void onResponse(Call<SignUpResponse> call, Response<SignUpResponse> response) {
                if(response.isSuccessful()){
                    Toast.makeText(SignUpActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(SignUpActivity.this,MainActivity.class);
                    intent.putExtra("NAME",signUp.getUsername());
                    startActivity(intent);
                }
            }

            @Override
            public void onFailure(Call<SignUpResponse> call, Throwable t) {
                Toast.makeText(SignUpActivity.this, "Can not create user", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }
    }
}
