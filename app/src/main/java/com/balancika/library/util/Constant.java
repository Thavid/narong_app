package com.balancika.library.util;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.balancika.library.R;

import java.util.Objects;

public class Constant {

    public static final String PROFILE = "lab_ame";
    public static final String BASE_URL = "http://103.205.26.55:8001/library-api/";

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public static void confirmDialog(Context context, String title, String description, final String confirm){
        //callback = (ConfirmDialogCallback) context;
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        @SuppressLint("InflateParams") View dialogView = inflater.inflate(R.layout.confirm_dialog, null);
        alertDialog.setView(dialogView);

        TextView tvTitle = dialogView.findViewById(R.id.tv_title);
        TextView tvDescription = dialogView.findViewById(R.id.tv_description);
        Button btnCancel = dialogView.findViewById(R.id.btn_done);
        Button btnConfirm = dialogView.findViewById(R.id.btn_confirm);

        tvTitle.setText(title);
        tvDescription.setText(description);
        btnConfirm.setText(confirm);
        Log.e("oooooooooooooo","llll");

        final AlertDialog dialog = alertDialog.create();
        Objects.requireNonNull(dialog.getWindow()).getAttributes().windowAnimations = R.style.DialogAnimation;
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //callback.confirm();
                dialog.dismiss();
            }
        });
        dialog.show();
    }
}
