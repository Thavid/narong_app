package com.balancika.library;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.balancika.library.adapter.SelectStudentAdapter;
import com.balancika.library.adapter.StudentAdapter;
import com.balancika.library.api.Api;
import com.balancika.library.response.StudentData;
import com.balancika.library.response.StudentResponse;
import com.balancika.library.retrofit.ServiceGenerator;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchActivity extends AppCompatActivity {

    @BindView(R.id.rv_item)
    RecyclerView rvItem;
    @BindView(R.id.progressBar2)
    ProgressBar progressBar;
    private SelectStudentAdapter adapter;
    private String type;
    private List<StudentData> listStudent = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        ButterKnife.bind(this);

        type = getIntent().getStringExtra("type");
        switch (type){
            case "selectStudent":
                adapter = new SelectStudentAdapter(this,listStudent);
                LinearLayoutManager layoutManager = new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false);
                rvItem.setLayoutManager(layoutManager);
                rvItem.setAdapter(adapter);
                Api.StudentApi api = ServiceGenerator.createService(Api.StudentApi.class);
                Call<StudentResponse> call = api.getStudent();
                call.enqueue(new Callback<StudentResponse>() {
                    @Override
                    public void onResponse(Call<StudentResponse> call, Response<StudentResponse> response) {
                        if(response.isSuccessful()){
                            listStudent.addAll(response.body().data);
                            adapter.notifyDataSetChanged();
                        }
                        else {
                            Toast.makeText(SearchActivity.this, getResources().getString(R.string.fail_message), Toast.LENGTH_SHORT).show();
                        }
                        progressBar.setVisibility(View.GONE);
                    }

                    @Override
                    public void onFailure(Call<StudentResponse> call, Throwable t) {
                        progressBar.setVisibility(View.GONE);
                    }
                });
        }
    }

    public void getStudent(int adapterPosition) {
        Intent returnIntent = new Intent();
        returnIntent.putExtra("STUDATA",listStudent.get(adapterPosition));
        setResult(RESULT_OK,returnIntent);
        finish();
    }
}
