package com.balancika.library;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.balancika.library.api.Api;
import com.balancika.library.request.AddStudentRequest;
import com.balancika.library.request.UpdateStudentRequest;
import com.balancika.library.response.AddStudentResponse;
import com.balancika.library.response.StudentData;
import com.balancika.library.retrofit.ServiceGenerator;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class StudentFormActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener, Validator.ValidationListener {
    @NotEmpty
    @BindView(R.id.ed_fitst_name)
    EditText edFirstName;
    @NotEmpty
    @BindView(R.id.ed_last_name)
    EditText edLastName;
    @NotEmpty
    @BindView(R.id.ed_address)
    EditText edAddresss;
    @NotEmpty
    @BindView(R.id.ed_phone)
    EditText edPhone;
    @BindView(R.id.toolbar_title)
    TextView tvToolbarTitle;
    @BindView(R.id.btn_done)
    TextView btnDone;
    @BindView(R.id.framelayout)
    Spinner genderSpinner;
    @BindView(R.id.progessbar)
    ProgressBar progressBar;
    String[] listGender={"Male","Female"};
    String gender;
    Validator validator;
    String type;
    StudentData studentData;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_form);
        ButterKnife.bind(this);
        progressBar.setVisibility(View.GONE);

        validator =  new Validator(this);
        validator.setValidationListener(this);

        tvToolbarTitle.setText("Register Student");
        btnDone.setVisibility(View.INVISIBLE);

        genderSpinner.setOnItemSelectedListener(this);
        ArrayAdapter aa = new ArrayAdapter(this,android.R.layout.simple_spinner_item,listGender);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        genderSpinner.setAdapter(aa);

        type = getIntent().getStringExtra("TYPE");
        if(type.equals("update")){
            studentData = (StudentData) getIntent().getSerializableExtra("STUDENT");
            edFirstName.setText(studentData.getFirstname());
            edLastName.setText(studentData.getLastname());
            gender = studentData.getGender();
            for(int i=0;i<listGender.length;i++){
                if(listGender[i].equals(studentData.getGender()))
                    genderSpinner.setSelection(i);
            }
            edAddresss.setText(studentData.getAddress());
            edPhone.setText(studentData.getTel());
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        gender = listGender[i];
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    @OnClick(R.id.btn_back)
    public void finishScreen(){
        finish();
    }
    @OnClick(R.id.btn_confirm)
    public void addStudent(){
        validator.validate();
    }

    @Override
    public void onValidationSucceeded() {
        progressBar.setVisibility(View.VISIBLE);
        if(type.equals("NEW")){
            AddStudentRequest addStudentRequest = new AddStudentRequest(edAddresss.getText().toString(),edFirstName.getText().toString(),gender,edLastName.getText().toString(),edPhone.getText().toString());
            Api.StudentApi api = ServiceGenerator.createService(Api.StudentApi.class);
            Call<AddStudentResponse> call = api.addStudent(addStudentRequest);
            call.enqueue(new Callback<AddStudentResponse>() {
                @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                @Override
                public void onResponse(Call<AddStudentResponse> call, Response<AddStudentResponse> response) {
                    if(response.isSuccessful()){
                        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(StudentFormActivity.this);
                        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                        @SuppressLint("InflateParams") View dialogView = inflater.inflate(R.layout.cu_success_dialog, null);
                        alertDialog.setView(dialogView);

                        TextView tvTitle = dialogView.findViewById(R.id.tv_title);
                        TextView tvDescription = dialogView.findViewById(R.id.tv_description);
                        Button btnConfirm = dialogView.findViewById(R.id.btn_confirm);

                        tvTitle.setText("Success");
                        tvDescription.setText(response.body().getMessage());
                        btnConfirm.setText("Ok");

                        final AlertDialog dialog = alertDialog.create();
                        Objects.requireNonNull(dialog.getWindow()).getAttributes().windowAnimations = R.style.DialogAnimation;

                        btnConfirm.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                //callback.confirm();
                                dialog.dismiss();
                                finish();
                            }
                        });
                        progressBar.setVisibility(View.GONE);
                        dialog.show();
                    }
                }

                @Override
                public void onFailure(Call<AddStudentResponse> call, Throwable t) {
                    Toast.makeText(StudentFormActivity.this, "Create student fail!", Toast.LENGTH_SHORT).show();
                    progressBar.setVisibility(View.GONE);
                }
            });
        }else {
            UpdateStudentRequest updateStudentRequest = new UpdateStudentRequest(edPhone.getText().toString(),edLastName.getText().toString(),studentData.getId(),gender,edFirstName.getText().toString(),edAddresss.getText().toString());
            Api.StudentApi api = ServiceGenerator.createService(Api.StudentApi.class);
            Call<AddStudentResponse> call = api.updateStudent(updateStudentRequest);
            call.enqueue(new Callback<AddStudentResponse>() {
                @Override
                public void onResponse(Call<AddStudentResponse> call, Response<AddStudentResponse> response) {
                    if(response.isSuccessful()){
                        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(StudentFormActivity.this);
                        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                        @SuppressLint("InflateParams") View dialogView = inflater.inflate(R.layout.cu_success_dialog, null);
                        alertDialog.setView(dialogView);

                        TextView tvTitle = dialogView.findViewById(R.id.tv_title);
                        TextView tvDescription = dialogView.findViewById(R.id.tv_description);
                        Button btnConfirm = dialogView.findViewById(R.id.btn_confirm);

                        tvTitle.setText("Success");
                        tvDescription.setText(response.body().getMessage());
                        btnConfirm.setText("Ok");

                        final AlertDialog dialog = alertDialog.create();
                        Objects.requireNonNull(dialog.getWindow()).getAttributes().windowAnimations = R.style.DialogAnimation;

                        btnConfirm.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                                finish();
                            }
                        });
                        progressBar.setVisibility(View.GONE);
                        dialog.show();
                    }else {
                        Toast.makeText(StudentFormActivity.this, "Update Failed", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<AddStudentResponse> call, Throwable t) {
                    Toast.makeText(StudentFormActivity.this, "Update Failed", Toast.LENGTH_SHORT).show();
                }
            });
        }
    }


    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }
    }
}
